<?php

namespace RomarkCode\Order\Contracts;

use RomarkCode\Order\Models\Order;

interface OrderItem
{
    public function getOrder(): Order;
}
