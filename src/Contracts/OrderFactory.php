<?php

namespace RomarkCode\Order\Contracts;

use RomarkCode\Cart\Models\Cart;

interface OrderFactory
{
    /**
     * Creates a new order from simple data arrays
     *
     * @param array $data
     * @param Cart $cart
     * @return Order
     */
    public function createFromCart(array $data, Cart $cart) : Order;
}
