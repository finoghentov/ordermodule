<?php

namespace RomarkCode\Order\Contracts;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use RomarkCode\Order\Models\BillingInfo;
use RomarkCode\Order\Models\ShippingInfo;
use Traversable;

interface Order
{
    public function items(): HasMany;

    public function user(): BelongsTo;

    public function getShippingInfo(): ShippingInfo;

    public function getBillingInfo(): BillingInfo;

    public function getItems(): Traversable;

    public function total(): int;
}
