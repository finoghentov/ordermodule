<?php

namespace RomarkCode\Order\Factories;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use RomarkCode\Cart\Models\Cart;
use RomarkCode\Order\Models\BillingInfo;
use RomarkCode\Order\Models\ShippingInfo;
use RomarkCode\Product\Contracts\Buyable;
use RomarkCode\Order\Contracts\Order;
use RomarkCode\Order\Contracts\OrderFactory as OrderFactoryContract;
use RomarkCode\Order\Exceptions\CreateOrderException;
use RomarkCode\Product\Models\Product;

class OrderFactory implements OrderFactoryContract
{
    /**
     * @inheritDoc
     */
    public function createFromCart(array $data, Cart $cart): Order
    {

        if (empty($cart->getItems())) {
            throw new CreateOrderException('Can not create an order without items');
        }

        DB::beginTransaction();

        try {
            $order = app(Order::class);
            $order->fill(Arr::except($data, ['shippingInfo', 'billingInfo']));
            $order->user_id = $data['user_id'] ?? auth()->id();
            $order->save();

            $this->createAddress($order, $data);

            $this->createItems($order,
                array_map(function ($item) {
                    // Default quantity is 1 if unspecified
                    $item['qty'] = $item['qty'] ?? 1;
                    return $item;
                }, $cart->getItems()->toArray())
            );

            $order->save();

        } catch (\Exception $e) {
            DB::rollBack();
            throw new CreateOrderException('Was an error while creating an order');
        }

        DB::commit();

        return $order;
    }


    /**
     * Make connection between buyable items and order
     *
     * @param Order $order
     * @param array $items
     */
    protected function createItems(Order $order, array $items)
    {
        foreach ($items as $item) {
            $this->createItem($order, $item);
        }
    }

    /**
     * Creates a single item for the given order
     *
     * @param Order $order
     * @param array $item
     */
    protected function createItem(Order $order, array $item)
    {
        if ($this->itemContainsABuyable($item)) {
            /** @var Buyable $product */
            $product = Product::findOrFail($item['product_id']);
            $item    = array_merge($item, [
                'product_id'   => $product->getId(),
                'price'        => $product->getPrice(),
            ]);
            unset($item['cart_id']);
        }
        $order->items()->create($item);
    }

    /**
     * Returns whether an instance contains a buyable object
     *
     * @param array $item
     *
     * @return bool
     */
    private function itemContainsABuyable(array $item) {
        $product = Product::find($item['product_id']);
        return isset($product) && $product instanceof Buyable;
    }


    /**
     * Creating address for order
     *
     * @param Order $order
     * @param array $data
     * @throws CreateOrderException
     */
    protected function createAddress(Order $order, array $data) {
        $info = $this->createAddressInfo($order, $data, 'shipping');
        ShippingInfo::create($info);


        if(!$order->hasSameAddresses()){
            $info = $this->createAddressInfo($order, $data, 'billing');
        }

        BillingInfo::create($info);
    }


    /**
     * Create address data
     *
     * @param Order $order
     * @param $data
     * @param $type
     * @return array
     * @throws CreateOrderException
     */
    private function createAddressInfo(Order $order, $data, $type){
        switch ($type){
            case 'billing':
                return array_merge($data['billingInfo'], ['order_id' => $order->id]);
            case 'shipping':
                return array_merge($data['shippingInfo'], ['order_id' => $order->id]);
            default:
                throw new CreateOrderException('Cannot create address info');
        }
    }
}
