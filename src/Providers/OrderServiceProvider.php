<?php

namespace RomarkCode\Order\Providers;

use Illuminate\Support\ServiceProvider;
use RomarkCode\Order\Contracts\OrderFactory as OrderFactoryContract;
use RomarkCode\Order\Factories\OrderFactory;
use RomarkCode\Order\Models\Order;
use RomarkCode\Order\Contracts\Order as OrderContract;
use RomarkCode\Order\Models\OrderItem;
use RomarkCode\Order\Contracts\OrderItem as OrderItemContract;
use RomarkCode\Order\Observers\OrderItemsObserver;
use RomarkCode\Order\Observers\OrderObserver;


class OrderServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '../database/migrations');

        $this->app->bind(OrderFactoryContract::class, OrderFactory::class);
        $this->app->bind(OrderContract::class, Order::class);
        $this->app->bind(OrderItemContract::class, OrderItem::class);

        Order::observe(OrderObserver::class);
        OrderItem::observe(OrderItemsObserver::class);
    }
}
