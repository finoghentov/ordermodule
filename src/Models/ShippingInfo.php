<?php

namespace RomarkCode\Order\Models;

use App\Models\State;
use Illuminate\Database\Eloquent\Model;

class ShippingInfo extends Model
{
    protected $guarded = [];

    public function order(){
        return $this->belongsTo(Order::class,'order_id');
    }

    public function state(){
        return $this->belongsTo(State::class, 'state_id');
    }
}
