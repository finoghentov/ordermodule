<?php

namespace RomarkCode\Order\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Traversable;
use RomarkCode\Order\Contracts\Order as OrderContract;

class Order extends Model implements OrderContract
{

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function shippingInfo(): HasOne
    {
        return $this->hasOne(ShippingInfo::class);
    }

    public function billingInfo(): HasOne
    {
        return $this->hasOne(BillingInfo::class);
    }

    public function items(): HasMany
    {
        return $this->hasMany(OrderItem::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @inheritdoc
     */
    public function getBillingInfo(): BillingInfo
    {
        return $this->billingInfo;
    }

    /**
     * @inheritdoc
     */
    public function getShippingInfo(): ShippingInfo
    {
        return $this->shippingInfo;
    }

    public function getItems(): Traversable
    {
        return $this->items;
    }

    public function total(): int
    {
        return $this->total_price;
    }

    public function hasSameAddresses(){
        return $this->same_address == 1;
    }
}
