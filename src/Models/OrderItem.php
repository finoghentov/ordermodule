<?php

namespace RomarkCode\Order\Models;

use Illuminate\Database\Eloquent\Model;
use RomarkCode\Order\Contracts\OrderItem as OrderItemContract;
use RomarkCode\Product\Models\Product;

class OrderItem extends Model implements OrderItemContract
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }


    public function getOrder(): Order{
        return $this->order;
    }


    public function total()
    {
        return $this->price * $this->qty;
    }
}
