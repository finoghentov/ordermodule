<?php

namespace RomarkCode\Order\Models;

use App\Models\State;
use Illuminate\Database\Eloquent\Model;

class BillingInfo extends Model
{
    protected $guarded = [];

    public function state(){
        return $this->belongsTo(State::class, 'state_id');
    }

    public function order(){
        return $this->belongsTo(Order::class, 'order_id');
    }
}
