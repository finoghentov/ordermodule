<?php

namespace RomarkCode\Order\Observers;


use RomarkCode\Order\Contracts\Order;

class OrderObserver extends OrderAction
{
    public function creating(Order $order){
        $order->total_price = $this->calculateTotal($order);
    }

    public function updating(Order $order){
        $order->total_price = $this->calculateTotal($order);
    }

    public function deleted(Order $order){
        $order->items()->delete();
    }
}
