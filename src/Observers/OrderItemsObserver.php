<?php

namespace RomarkCode\Order\Observers;

use RomarkCode\Order\Contracts\OrderItem;
use RomarkCode\Product\Models\Product;

class OrderItemsObserver extends OrderAction
{
    public function creating(OrderItem $item){
        $product = Product::findOrFail($item->product_id);
        $item->price = $product->price;
        $item->total_price = $product->price * $item->qty;
    }

    public function updating(OrderItem $item){
        $product = Product::findOrFail($item->product_id);
        $item->price = $product->price;
        $item->total_price = $product->price * $item->qty;
    }

    public function created(OrderItem $item){
        $item->getOrder()->total_price = $this->calculateTotal($item->order);
        $item->getOrder()->save();
    }

    public function updated(OrderItem $item){
        $item->getOrder()->total_price = $this->calculateTotal($item->order);
        $item->getOrder()->save();
    }

    public function deleted(OrderItem $item){
        $item->getOrder()->total_price = $this->calculateTotal($item->order);
        $item->getOrder()->save();
    }
}
