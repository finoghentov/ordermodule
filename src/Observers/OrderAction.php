<?php


namespace RomarkCode\Order\Observers;


use RomarkCode\Order\Contracts\Order;

class OrderAction
{
    protected function calculateTotal(Order $order){

        if($order->getItems()->isEmpty()){
            return 0;
        }

        $total_price = $order->items()->sum('total_price');

        return $this->calculateWithPromocode($order->promocode, $total_price);
    }

    private function calculateWithPromocode($promocode, $price){
        if(!$promocode){
            return $price;
        }

        switch ($promocode->sale_type){
            case 'cash':
                return $price - $promocode->amount;
            case 'percent':
                return $price - $price / 100 * $promocode->amount;
        }
    }
}
