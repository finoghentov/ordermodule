<?php


namespace App\Order\tests;

use RomarkCode\Cart\Facades\Cart as CartFacade;
use RomarkCode\Cart\Models\Cart;
use RomarkCode\Cart\Models\CartItem;
use RomarkCode\Order\Contracts\Order;
use RomarkCode\Order\Contracts\OrderFactory;
use RomarkCode\Order\Models\BillingInfo;
use RomarkCode\Order\Models\ShippingInfo;
use RomarkCode\Product\Contracts\Buyable;
use Tests\TestCase;

class OrderTest extends TestCase
{
    protected $cart;

    protected function setUp(): void
    {
        parent::setUp();
        $pathToFactories = __DIR__.'/Factories';
        $factory = \Illuminate\Database\Eloquent\Factory::construct(
            \Faker\Factory::create(),
            $pathToFactories
        );

        $this->cart = $factory->of(Cart::class)->create();
        session(['cart_id' => $this->cart->id]);
        $factory->of(CartItem::class)->times(5)->create(['cart_id' => $this->cart->id]);
    }

    /**
     * @test
     * @group Order
     */
    public function testCreateOrderFromCart(){
        $factory = app(OrderFactory::class);

        $data = [
            'session_id' => session()->getId(),
            'same_address' => 1,
            'shippingInfo' => [
                'full_name' => 'Pavel',
                'phone' => '373',
                'address' => 'Fedko',
                'apartment' => '39',
                'city' => 'Tiraspol',
                'state_id' => 2,
                'zip' => '3300'
            ]
        ];

        $order = $factory->createFromCart($data, CartFacade::model());
        $this->assertTrue(true, $order instanceof Order);
        $order->load('items');
        return $order;
    }

    /**
     * @test
     * @group Order
     */
    public function testOrderItemsCreating(){
        $order = $this->testCreateOrderFromCart();
        $this->assertTrue(true, $order->getItems()->isNotEmpty());
    }

    /**
     * @test
     * @group Order
     */
    public function getOrderAddresses(){
        $order = $this->testCreateOrderFromCart();
        $this->assertTrue(true, $order->getShippingInfo() instanceof ShippingInfo);
        $this->assertTrue(true, $order->getBillingInfo() instanceof BillingInfo);
    }

    /**
     * @test
     * @group Order
     */
    public function testOrderItemsBelongsToOrder(){
        $order = $this->testCreateOrderFromCart();
        $order->load('items');
        $item = $order->getItems()[0];

        $this->assertEquals($item->getOrder()->id, $order->id);
    }

    /**
     * @test
     * @group Order
     */
    public function testOrderItemsBelongsToProduct(){
        $order = $this->testCreateOrderFromCart();
        $order->load('items');
        $item = $order->getItems()[0];

        $this->assertTrue(true, $item instanceof Buyable);
    }
}
