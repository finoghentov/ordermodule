<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use Faker\Generator as Faker;
use RomarkCode\Cart\Models\Cart;
use RomarkCode\Cart\Models\CartItem;
use RomarkCode\Product\Models\Product;

$factory->define(CartItem::class, function (Faker $faker, $attributes) {
    $product = Product::inRandomOrder()->first();
    return [
        'cart_id' => $attributes['cart_id'],
        'product_id' => $product->id,
        'price' => $product->price,
        'qty' => rand(1, 5)
    ];
});
